FROM docker-registry.wikimedia.org/golang:1.13-3
USER root
RUN apt-get update && apt-get install -y curl
USER nobody
