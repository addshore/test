# Testing Gitlab runners

## VM Setup

I made a VM: `gitlab-runner-addshore-1001.integration.eqiad1.wikimedia.cloud`

### Install docker

```sh
sudo apt-get remove docker docker-engine docker.io containerd runc
sudo apt-get update
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    lsb-release

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo \
  "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

### Install gitlab runner

```sh
curl -LJO "https://gitlab-runner-downloads.s3.amazonaws.com/latest/deb/gitlab-runner_amd64.deb"
sudo dpkg -i gitlab-runner_amd64.deb
```

### Register runners..

Note: concurrent vs limit https://stackoverflow.com/a/51836770/4746236

So register 2 runners (one per project), which can take 6 jobs each

```sh

# For addshore/test
sudo gitlab-runner register -n \
  --url https://gitlab.wikimedia.org/ \
  --registration-token XXXaddshore-test-tokenXXX \
  --executor docker \
  --limit 6 \
  --name "gitlab-runner-addshore-1001-docker-0011" \
  --docker-image "docker:19.03.12" \
  --docker-privileged \
  --docker-volumes "/certs/client"

# For releng/mwcli
sudo gitlab-runner register -n \
  --url https://gitlab.wikimedia.org/ \
  --registration-token XXXreleng-mwcli-tokenXXX \
  --executor docker \
  --limit 6 \
  --name "gitlab-runner-addshore-1001-docker-0012" \
  --docker-image "docker:19.03.12" \
  --docker-privileged \
  --docker-volumes "/certs/client"
```

### Configure "global" runner jobs

Allow 6 jobs at once globally on this runner and restart gitlab runner

```sh
sudo sed -i 's/^concurrent =.*/concurrent = 6/' "/etc/gitlab-runner/config.toml"
sudo systemctl restart gitlab-runner
```

### Register the Google docker mirror (OPTIONAL)

```sh
sudo echo '{"registry-mirrors": ["https://mirror.gcr.io"]}' > /etc/docker/daemon.json
sudo service docker restart
```

Check with:

```sh
docker system info
```

You should see:

```
 Registry Mirrors:
  https://mirror.gcr.io/
```

Also add the mirror for dind in `/etc/gitlab-runner/config.toml` to each runner it is needed for

https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#enable-registry-mirror-for-dockerdind-service

```sh
    [[runners.docker.services]]
      name = "docker:19.03.12-dind"
      command = ["--registry-mirror", "https://mirror.gcr.io"]
```

And restart the gitlab runner service:

```sh
sudo systemctl restart gitlab-runner
```

### Use custom docker mirror

Mainly from https://about.gitlab.com/blog/2020/10/30/mitigating-the-impact-of-docker-hub-pull-requests-limits/

Create a mirror (using docker):

```sh
sudo docker run -d -p 6000:5000 \
    -e REGISTRY_PROXY_REMOTEURL=https://registry-1.docker.io \
    --restart always \
    --name registry registry:2
```

Get the IP address:

```ssh
hostname --ip-address
```

Add the mirror in the same way as for Google, but with the custom IP!
